module.exports = {
  transpileDependencies: ['vuetify'],
  configureWebpack: {
    devServer: {
      // https://webpack.js.org/configuration/dev-server/#devserverproxy
      proxy: {
        '/api': {
          target: 'http://localhost:5000',
        },
      },
    },
  },
}
