import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: require('@/pages/Home.vue').default,
    meta: {
      auth: true,
    },
    alias: '',
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '@/pages/Login.vue'),
    meta: {
      layout: 'no-sidebar',
    },
  },
  {
    path: '/browse',
    name: 'Browse',
    component: () => import(/* webpackChunkName: "browse" */ '@/pages/Browse.vue'),
    meta: {
      auth: true,
    },
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '@/pages/About.vue'),
    meta: {
      layout: 'no-sidebar',
    },
  },
]

export default new VueRouter({
  mode: 'history',
  routes,
})
