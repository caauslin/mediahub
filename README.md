# Media Hub

## Development

#### Prerequisites

- python 3.7+
- pipenv
- node
- yarn

#### Server Setup

To install python dependencies

```shell script
pipenv install
```

Then run following command for activating the virtual environment

```shell script
pipenv shell
```

To start the development server

```shell script
export FLASK_APP=mediahub
export FLASK_ENV=development
flask run
```

OR in Windows PowerShell

```powershell
$env:FLASK_APP = "mediahub"
$env:FLASK_ENV = "development"
flask run
```

Use `flask run --help` to see more options for controlling the dev server.

> It is recommended to use `flask run` over `Flask.run()` in code for **development** due to how the reload mechanism works. For more details, see [this docs](https://flask.palletsprojects.com/en/1.1.x/server/).

#### Client Setup

To install node dependencies

```shell script
yarn install
```

To start the development server

```shell script
yarn serve
```

To lint the code, append `--no-fix` to disable automatically fixing warnings or errors
```
yarn lint
```

#### Vue CLI Configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

#### Configure ESLint & Auto Formatting in VS Code

Make sure following extensions are installed:
- `Vetur`
- `Prettier — Code formatter`
- `ESLint`

Then add following lines to workspace settings in `.vscode/settings.json`:

```json
{
  "editor.formatOnSave": false,
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
  "vetur.validation.template": false
}
```

> `Vetur` comes with a default formatter that might conflict with `Prettier — Code formatter`, it can be fixed by setting the default format using the `Command Palette` (activate it with `Ctrl` + `Shift` + `P`), then search for `Format Document With`, and set `Prettier — Code formatter` as default formatter for consistency (*`eslint` is using `prettier`*).
