module.exports = {
  root: true,
  env: {
    node: true,
    browser: true,
  },
  extends: [
    'plugin:vue/recommended',
    'eslint:recommended',
    'prettier/vue',
    'plugin:prettier/recommended',
  ],
  rules: {
    // don't throw errors for unused variables
    'vue/no-unused-components': 'warn',
    'no-unused-vars': 'warn',
    // no debug message in production environment
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // see https://prettier.io/docs/en/options.html
    'prettier/prettier': [
      'warn',
      {
        singleQuote: true,
        semi: false,
        quoteProps: 'consistent',
        printWidth: 100,
      },
    ],
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
}
