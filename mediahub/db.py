import os
import sqlite3
import uuid
from flask import current_app as app, g
from mediahub.util import hash_password


def get_database():
    if 'database' not in g:
        g.database = sqlite3.connect(
            app.config['DATABASE_URI'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.database.row_factory = sqlite3.Row
    return g.database


@app.teardown_appcontext
def close_database(exception=None):
    database = g.pop('database', None)
    if database is not None:
        database.close()


def init_database():
    if not os.path.isabs(app.config['DATABASE_URI']):
        app.config['DATABASE_URI'] = os.path.join(app.instance_path, app.config['DATABASE_URI'])
        app.logger.info('DATABASE is set to a relative path, it will be resolved under instance folder')

    if not os.path.exists(app.config['DATABASE_URI']):
        seed_database()


def seed_database():
    """Caution: this function clears everything in the database if exists."""
    app.logger.info('Seeding database ...')
    conn = get_database()
    with app.open_resource('schema.sql', mode='r') as schema_file:
        schema_sql = schema_file.read()
        cur = conn.cursor()
        cur.executescript(schema_sql)
        conn.commit()
    # add default user
    app.logger.info('Adding super user: admin ...')
    user_id = uuid.uuid4().hex
    hashed_password = hash_password('password')
    conn = get_database()
    cur = conn.cursor()
    cur.execute(
        'INSERT INTO "user" VALUES (?, ?, ?)',
        (user_id, 'admin', hashed_password)
    )
    conn.commit()
