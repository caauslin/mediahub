import hashlib
import jwt
from flask import current_app as app


def hash_password(password: str, salt: str = None) -> str:
    """Return hashed password."""
    if len(password) >= 1024:
        raise ValueError('Length of the password to be hashed must be less than 1024')

    return hashlib.pbkdf2_hmac(
        'sha256',
        password.encode('utf-8'),
        # salt should be about 16 or more bytes from a proper source, e.g. os.urandom()
        (salt or app.config['SECRET_KEY']).encode('utf-8'),
        # as of 2013, at least 100,000 iterations of SHA-256 are suggested
        100000
    ).hex()


def jwt_encode(payload: dict, key: str = None) -> str:
    """Return a JWT encoded string of specified dict."""
    return jwt.encode(payload, key or app.config['SECRET_KEY'], algorithm='HS256').decode('utf-8')


def jwt_decode(token: str, key: str = None) -> dict:
    """Decode a JWT encoded string to its the original data."""
    return jwt.decode(token, key or app.config['SECRET_KEY'], algorithms=['HS256'])
