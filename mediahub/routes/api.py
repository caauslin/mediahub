from datetime import datetime
from flask import Blueprint, request, session, send_file
from mediahub.db import get_database
from mediahub.core import require_json, require_auth, scan_dir, resolve_path
from mediahub.util import hash_password, jwt_encode


blueprint = Blueprint('api', __name__, url_prefix='/api')


@blueprint.route('/echo', methods=['GET'])
def echo():
    return {'ts': int(datetime.utcnow().timestamp())}


@blueprint.route('/login', methods=['POST'])
@require_json
def login():
    username = request.json['username']
    password = hash_password(request.json['password'])
    cur = get_database().cursor()
    cur.execute('SELECT * FROM "user" WHERE username=? LIMIT 1', (username,))
    row = cur.fetchone()
    if row is None or password != row['password']:
        return {'error': 'Incorrect username or password'}, 401
    token = jwt_encode({'user_id': row['id']})
    session['token'] = token
    return {'token': token}


@blueprint.route('/logout', methods=['GET'])
@require_auth
def logout(_):
    session['token'] = None
    return {}


@blueprint.route('/files/browse', methods=['POST'])
@require_json
@require_auth
def browse_files(_):
    path = request.json.get('path', '.')
    try:
        result = [e for e in scan_dir(path)]
        return {'entries': result}
    except PermissionError:
        return {'error': 'You do NOT have permission to view this folder'}, 404
    except FileNotFoundError:
        return {'error': 'Folder does NOT exist'}, 404


@blueprint.route('/files/fetch', methods=['GET'])
@require_auth
def view_file(_):
    path = request.args.get('path', None)
    if path is None:
        return {'error': 'Missing file path'}, 404
    try:
        filepath = resolve_path(path)
        return send_file(filepath)
    except FileNotFoundError:
        return {'error': 'File does NOT exist'}, 404
