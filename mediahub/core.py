import functools
import os
from flask import current_app as app, request, session
from mediahub.util import jwt_decode


def require_json(fn):
    """Make sure the content-type of the request is application/json."""
    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        content_type = request.headers.get('Content-Type', None)
        if content_type.split(';')[0] != 'application/json':
            return {'error': 'Unsupported content type, expect JSON'}, 400
        return fn(*args, **kwargs)
    return wrapper


def require_auth(fn):
    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        token = session.get('token', None)
        if token is None:
            return {'error': 'Authentication required'}, 401
        try:
            identity = jwt_decode(token)
        except Exception as e:
            app.logger.warning('An error occurred when decoding JWT token')
            app.logger.warning(e)
            return {'error': 'Invalid authentication token'}, 401
        return fn(*args, identity, **kwargs)
    return wrapper


def scan_dir(path, root_paths=None):
    root_paths = root_paths or app.config['MEDIA_PATHS']
    if len(root_paths) == 0:
        return
    path = os.path.normpath(path).lstrip(os.sep)
    if len(root_paths) == 1:
        base_path = os.path.abspath(root_paths[0])
        scan_path = os.path.join(base_path, path)
        yield from _scan_dir(scan_path, base_path)
    elif path == '.':
        for root_path in root_paths:
            root_name = os.path.basename(os.path.normpath(root_path))
            yield {'name': root_name, 'type': 'folder', 'path': f'/{root_name}'}
    else:
        target_root = path.split(os.sep)[0]
        for root_path in root_paths:
            root_path = os.path.abspath(root_path)
            base_name = os.path.basename(root_path)
            if base_name != target_root:
                continue
            base_path = os.path.dirname(root_path)
            scan_path = os.path.join(base_path, path)
            yield from _scan_dir(scan_path, base_path)
            return
        raise FileNotFoundError()


def _scan_dir(scan_path, base_path):
    rel_path = os.path.relpath(scan_path, start=base_path)
    if rel_path.startswith('..'):
        raise FileNotFoundError()
    elif rel_path == '.':
        rel_path = ''
    with os.scandir(scan_path) as entries:
        for entry in entries:
            try:
                item = dict()
                item['name'] = entry.name
                if entry.is_dir():
                    item['type'] = 'folder'
                elif entry.is_file():
                    item['type'] = 'file'
                else:
                    continue
                item['path'] = os.path.join(rel_path, entry.name).replace('\\', '/')
                yield item
            except OSError:
                continue


def resolve_path(path, root_paths=None):
    root_paths = root_paths or app.config['MEDIA_PATHS']
    if len(root_paths) == 0:
        return
    path = os.path.normpath(path).lstrip(os.sep)
    if len(root_paths) == 1:
        return os.path.join(root_paths[0], path)
    elif path == '.':
        return None
    else:
        target_root = path.split(os.sep)[0]
        for root_path in root_paths:
            root_path = os.path.abspath(root_path)
            base_name = os.path.basename(root_path)
            if base_name != target_root:
                continue
            base_path = os.path.dirname(root_path)
            return os.path.join(base_path, path)
        raise FileNotFoundError()
