import click
from mediahub import create_app


@click.group()
def cli():
    pass


@cli.command('init')
def init():
    import os
    from flask import current_app as app
    with create_app().app_context():
        config_path = os.path.join(app.instance_path, 'config.json')
        click.echo(f'Config path: "{config_path}"')


@cli.command('hash')
@click.argument('password', type=str)
def hash_password(password):
    from mediahub.util import hash_password
    with create_app().app_context():
        click.echo(hash_password(password))


if __name__ == '__main__':
    cli()
