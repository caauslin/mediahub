DROP TABLE IF EXISTS "user";
CREATE TABLE "user" (
  "id"	TEXT NOT NULL UNIQUE,
  "username"	TEXT NOT NULL UNIQUE,
  "password"	TEXT,
  PRIMARY KEY("id")
);
