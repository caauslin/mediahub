import os
from flask import Flask


def create_app(override_config=None):
    app = Flask(__name__, instance_relative_config=True)

    # set default config
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE_URI='default.db',
        MEDIA_PATHS=[os.path.expanduser('~')]
    )
    # load instance config from instance folder
    app.config.from_json('config.json', silent=True)
    # override config
    if override_config is not None:
        app.config.update(override_config)

    # make sure instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # ensure current_app is available
    with app.app_context():
        # make sure event/signal handling functions are registered properly
        from mediahub import db
        db.init_database()

        # register blueprints
        from mediahub.routes import api
        app.register_blueprint(api.blueprint)

    return app
